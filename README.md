# TER : Timeline Escape Rooms

Build a simple timeline based on your escape games adventures.
## Installation

```bash
yarn install
```

## TODO List

- [x] Display a simple timeline through an example file
- [x] Use hours
- [x] Manage doubleClick event to go to escape room's website
- [x] Different color according if you manage to escape the room or not
- [x] Display date when user click on escape rectangle
- [x] Display screenshot on README
- [ ] Update README for user
- [ ] Update README for developpers

## Example

![](doc/screenshot.png)
