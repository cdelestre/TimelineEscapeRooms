const webpack = require("webpack");
const path = require("path");
const vis = require("vis");
const jquery = require("jquery");
const UglifyJSPlugin = require("uglifyjs-webpack-plugin");

let config = {
  entry: "./src/index.js",
  output: {
    path: path.resolve(__dirname, "./public"),
    filename: "./escapesTimeline.js"
  },
  mode: 'development',
  plugins: [
      new UglifyJSPlugin()
    ],
}


module.exports = config;
