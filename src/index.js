import $ from 'jquery';
import vis from 'vis';

// DOM element where the Timeline will be attached
// Vis doesn't like JQuery object oO
var container = document.getElementById('visualization');
// Create a DataSet (allows two way data-binding)
var items = [];
var wonEscape = 0;
var userInfoDiv = $('#user-info');
var escapeInfoDiv = $('#escape-info');
var fileName = 'myEscapes.json';

$.getJSON(fileName, function (data) {
//  $.getJSON("escape_example.json", function (data) {
    $.each(data, function (index, value) {
      var europeFormatDate = value.date.split('/');
      var europeFormatHours = value.hours.split(':');
      var dateTmp = new Date(europeFormatDate[2], europeFormatDate[1]-1, europeFormatDate[0], europeFormatHours[0], europeFormatHours[1], '00');
      if (value.won == "True"){
        items.push({id: index, content: value.adventureName+' - '+value.name+' ('+value.city+')', start: dateTmp, website:value.website, won:true});
        wonEscape++;
      }
      else{
        items.push({id: index, content: value.adventureName+' - '+value.name+' ('+value.city+')', start: dateTmp, website:value.website, className:'red', won:false});

      }
    });
    renderTimeline(items, container);
    fillUserInfo(items, items.length, wonEscape, userInfoDiv);
});

function renderTimeline(items, container){
  var dataSet =  new vis.DataSet(items);
  // Configuration for the Timeline
  var options = {};
  // Create a Timeline
  var timeline = new vis.Timeline(container, dataSet, options);
  timeline.on('doubleClick', function (properties) {
    var urlToGo = items[properties.item].website;
    window.open(urlToGo, '_blank');
  });
  timeline.on('click', function (properties) {
    escapeInfoDiv.empty();
    var dateToDisplay = properties.time.getDate()+"/"+properties.time.getMonth()+"/"+properties.time.getFullYear();
    if(items[properties.item].won){
      var resultToDisplay = 'You won the escape'
    }
    else{
      var resultToDisplay = 'You lose the escape'
    }
    escapeInfoDiv.append('<p>'+resultToDisplay+' the '+dateToDisplay+'</p>');
  });
}

function fillUserInfo(items, totalEscape, wonEscape, userInfoDiv){
  var wonAverage = wonEscape/(totalEscape*100);
  var wonAverage = parseFloat(wonEscape*100 / totalEscape).toFixed(2);
  var wonAverageSentence = 'You manage to escape to '+wonEscape+' on '+totalEscape+' escape games, which gives you a success rate of '+wonAverage+' %';
  userInfoDiv.append('<span>'+wonAverageSentence+'</span>');
}
