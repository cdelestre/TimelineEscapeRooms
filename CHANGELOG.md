# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.0] - 2018-11-07
### Added
- CHANGELOG file

### Changed
- README file
- Escape info when user click on escape rectangle
- Add screenshot on README


## [1.0.0] - 2018-05-07
### Added
- First working version : user can import a JSON file and will see his escape timeline. He also can double click on an escape rectangle to go to escape's website (see README file).
